
package ejercicio6;


/**
 *
 * @author nacho
 */
public class Main {
     public static void main(String[] args) throws Exception  {
         Aeropuerto ae1=new Aeropuerto("Manises");
         for (int i = 1; i <=3; i++) {
             // Me da lo mismo repetir datos pues tengo un identificador único
             // que se crea automáticamente: avion1, avion2, etc.
             Avion av=new Avion("737", 913, "Boeing"); 
             ae1.añadirAvion(av);
         }
         ae1.listarAviones();
         Avion av2=ae1.despegar("avion2");
         ae1.listarAviones();
         System.out.println("\nVolando");
         System.out.println("-------");
         System.out.println(av2.toString());
         System.out.println("\nNúmero de Aviones: " + Avion.getNumAviones());
         

    }
}

 
