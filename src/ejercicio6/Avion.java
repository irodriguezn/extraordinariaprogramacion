/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio6;

/**
 *
 * @author nacho
 */
public class Avion extends Vehiculo implements Volador {
    
    private static int numAviones;
    private String idAvion;
    private String tipo;
    private String estado;
    
    
    public Avion(String modelo, int velocidad, String tipo) {
        super(modelo, velocidad);
        this.tipo=tipo;
        numAviones++;
        idAvion="avion"+numAviones;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public void aterrizar() {
        estado="en pista";
    }

    @Override
    public void despegar() {
        estado="volando";
    }

    public String getIdAvion() {
        return idAvion;
    }

    @Override
    public String toString() {
        return super.toString() + " Avion{" + "idAvion=" + idAvion + ", tipo=" + tipo + ", estado=" + estado + '}';
    }

    public static int getNumAviones() {
        return numAviones;
    }
    
    
}
