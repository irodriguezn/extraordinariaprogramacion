
package ejercicio6;

import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author nacho
 */
public class Aeropuerto {
    private String nombre;
    private LinkedList<Avion> aviones=new LinkedList<>();

    public Aeropuerto(String nombre) {
        this.nombre = nombre;
    }
    
    public void añadirAvion(Avion av) {
        av.aterrizar();
        aviones.add(av);
    }
    
    public Avion despegar(String idNombre) {
        boolean encontrado=false;
        Avion AvBuscado=null;
        Iterator it=aviones.iterator();
        while (it.hasNext()) {
            AvBuscado=(Avion)it.next();
            if (AvBuscado.getIdAvion().equals(idNombre)) {
                AvBuscado.despegar();
                aviones.remove(AvBuscado);
                break;
            }
        }
        return AvBuscado;
    }
    
    // Método extra que no pedía pero que viene bien
    public void listarAviones() {
        System.out.println("\nAeropuerto de " + nombre);
        System.out.println("----------------------");
        for (Avion av:aviones) {
            System.out.println(av.toString());
        }
    }
    
}
