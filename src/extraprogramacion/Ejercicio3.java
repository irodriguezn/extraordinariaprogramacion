
package extraprogramacion;

/**
 *
 * @author nacho
 */
public class Ejercicio3 {
    public static void main(String[] args) {
        trianguloInvHueco(7,"*");
    }
    
    static void trianguloInvHueco(int base, String car) {
        for (int i=1; i<=base; i++) {
            System.out.print(car);
        }
        System.out.println("");
        for (int i=base-1; i>1; i--) {
            if (i>2) {
                System.out.print(car);
                for (int j = 1; j<=i-2; j++) {
                    System.out.print(" ");
                }
                System.out.print(car);
            } else {
                System.out.print(car+car);
            }
            System.out.println("");
        }
        System.out.println(car);
    }
}
