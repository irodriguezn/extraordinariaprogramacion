
package extraprogramacion;

/**
 *
 * @author nacho
 */
public class Ejercicio2 {
    
    public static void main(String[] args) {
        System.out.println(getNewName(new String[]{"pepe", "pepe17"}, "pepe"));
    }
    
    static String getNewName(String[] dir, String nombre) {
        String nombreValido=nombre;
        boolean esValida=false;
        while (! esValida) {
            if (existeNombre(dir, nombreValido)) {
                nombreValido+=(int)(Math.random()*50+1);
            } else {
                esValida=true;
            }
        }
        return nombreValido;
    }
    
    static boolean existeNombre(String [] dir, String nombre) {
        boolean existeNombre=false;
        for (int i=0; i<dir.length; i++) {
            if (dir[i].equalsIgnoreCase(nombre)) {
                existeNombre=true;
                break;
            }
        }
        return existeNombre;
    }
}
