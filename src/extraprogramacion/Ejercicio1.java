
package extraprogramacion;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

/**
 *
 * @author nacho
 */
public class Ejercicio1 {

    
    public static void main(String[] args) {
     
        try {
            int[][] tablero= leeTablero("tablero.txt");
            for (int fila[]:tablero) {
                for (int num:fila) {
                    System.out.print(num+" ");
                }
                System.out.println("");
            }
        } catch (FileNotFoundException e) {
            System.out.println("No se encuentra el archivo");  
        } catch (IOException e) {
            System.out.println("Error entrada/salida");
        }
        
    }
    
    static int[][] leeTablero(String nombre) throws FileNotFoundException, IOException {
        String linea;
        int filas, columnas;
        StringTokenizer st;
        FileReader fr= new FileReader(nombre);
        BufferedReader br= new BufferedReader(fr);
        
        // Leemos la primera línea para saber el número de filas y columnas
        linea=br.readLine();
        int pos = linea.indexOf(" ");
        filas=Integer.parseInt(linea.substring(0, pos));
        columnas=Integer.parseInt(linea.substring(pos+1));
        int tablero[][]=new int[filas][columnas];
        // Ahora vamos a ir leyendo tantas líneas como filas
        linea=br.readLine();
        for (int i=0; i<filas; i++) {
            st=new StringTokenizer(linea);
            for (int j=0; j<columnas; j++) {
                tablero[i][j]=Integer.parseInt(st.nextToken(";"));
            }
            linea=br.readLine();
        }
        return tablero;
    }
}
