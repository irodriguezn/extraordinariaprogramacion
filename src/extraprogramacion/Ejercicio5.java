
package extraprogramacion;

import java.util.StringTokenizer;

/**
 *
 * @author nacho
 */
public class Ejercicio5 {
    public static void main(String[] args) {
        String matriz[][]=frecuenciaPalabras("El hombre es un lobo para el hombre");
        for (String[] v:matriz) {
            for (String ele:v) {
                System.out.print(ele+" ");
            }
            System.out.println("");
        }
    }
    
    static String[][] frecuenciaPalabras(String cadena) {
        String pal;
        int contador=0;
        int [] frecuencias =new int[100];
        String [] palabras=new String[100];
        StringTokenizer st=new StringTokenizer(cadena);
        while (st.hasMoreTokens()) {
            pal=st.nextToken();
            int i=0;
            boolean encontrado=false;
            while (palabras[i]!=null) {
                if (palabras[i].equalsIgnoreCase(pal)) {
                    encontrado=true;
                    frecuencias[i]++;
                    break;
                }
                i++;
            }
            if (!encontrado) {
                palabras[i]=pal;
                frecuencias[i]++;
                contador++;
            }
        }
        String matriz[][]=new String[2][contador];
        for (int i=0; i<contador; i++) {
            matriz[0][i]=frecuencias[i]+"";
            matriz[1][i]=palabras[i];
        }
        return matriz;
    }
}
