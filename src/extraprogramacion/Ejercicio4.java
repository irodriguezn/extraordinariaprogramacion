
package extraprogramacion;

/**
 *
 * @author nacho
 */
public class Ejercicio4 {
    public static void main(String[] args) {
        cuadrados(5, 4);
    }
    
    static void cuadrados(int num, int lado) {
        for (int i=1; i<=lado; i++) {
            for (int j=1; j<=num; j++) {
                for (int k=1; k<=lado; k++) {
                    System.out.print("* ");
                }
                System.out.print("   ");
            }
            System.out.println("");
        }
        System.out.println("");
    }
}
