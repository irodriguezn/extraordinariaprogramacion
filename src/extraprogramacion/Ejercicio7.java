/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extraprogramacion;

import java.util.Scanner;

/**
 *
 * @author nacho
 */
public class Ejercicio7 {
    public static void main(String[] args) {
        String nombre=validaCadena("pepe, ", 10, "Introduce tu nombre: ");
        System.out.println(nombre);
    }
    
    static String validaCadena(String caracteresValidos, int max, String msj) {
        String palabra="";
        boolean caracterNoValido=false;
        // Vamos a usar un booleano como indicador de si la palabra es o no 
        // válida. En principio supondremos que no lo es para que entre en el
        // bucle y siga en él hasta que veamos que la palabra que nos ha dado
        // el usuario es válida y si no seguiremos preguntándosela.
        boolean esValida=false;
        Scanner sc=new Scanner(System.in);
        // Mientras la palabra sea no válida seguiremos pidièndola
        while (!esValida) {
            System.out.print(msj);
            palabra=sc.nextLine();
            // Vemos si cumple la primera condición para ser válida
            if (palabra.length()>max) {
                // si no cumple se lo hacemos saber al usuario antes de volver
                // a pedirle la cadena.
                System.out.println("La cadena tiene que tener como máximo " + max + " caracteres");
            } else { // El tamaño lo cumple, pero todos los caracteres son válidos?
                // Vamos a comprobar ahora si todos los caracteres de la cadena
                // introducida por el usuario son válidos. Es decir, tenemos que
                // recorrer uno a uno todos los caracteres de la cadena y
                // ver si pertenecen a los caracteres válidos.
                for (int i=0; i<palabra.length(); i++) {
                    char letra=palabra.charAt(i);
                    String letraMin=(letra+"").toLowerCase();
                    caracterNoValido=false;
                    if (caracteresValidos.indexOf(letraMin)==-1) {
                        // Si alguno de los caracteres es no válido entonces
                        // la palabra no lo es, así que se lo indicamos al usuario
                        System.out.println("El caracter " + letra + " es un caracter no válido");
                        caracterNoValido=true;
                        break;
                    }
                }
                if (!caracterNoValido) {
                    esValida=true;
                };
            }
        }
        return palabra;        
    }
    
}
